﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace MainVault.View
{
    public class NullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NullToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool flip = false;
            if (parameter != null)
            {
                var p = (string)parameter;
                flip = string.Compare("Flip", p, true) == 0;
            }
            var d = value == null;
            d = flip ? !d : d;
            return d ;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    public class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                bool flip = false;
                if (parameter != null)
                {
                    var p = (string)parameter;
                    flip = string.Compare("Flip", p, true) == 0;
                }
                var d = (bool)value;
                d = flip ? !d : d;
                return d ? Visibility.Visible : Visibility.Collapsed;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented");
        }
    }

    public class CurrentViewToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                var d = (ViewModel.ViewModel.ViewEnum)value;
                bool b = false;
                if (parameter != null)
                {
                    var p = (string)parameter;
                    b = string.Compare(d.ToString(), p, true) == 0;
                }
                
                return b ? Visibility.Visible : Visibility.Collapsed;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented");
        }
    }


    public class KeyValueStateEnumToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                bool flip = false;
                if (parameter != null)
                {
                    var p = (string)parameter;
                    flip = string.Compare("Flip", p, true) == 0;
                }
                var ee = (MainVault.ViewModel.ViewModel.ViewEditAddStateEnum)value;
                //visible if in edit or add state
                var b = (ee == MainVault.ViewModel.ViewModel.ViewEditAddStateEnum.Edit || ee == MainVault.ViewModel.ViewModel.ViewEditAddStateEnum.Add);
                b = flip ? !b : b;  //negate if flip
                return b ? Visibility.Visible : Visibility.Collapsed;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented");
        }
    }

    public class ViewEditAddStateEnumToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                var ee = (MainVault.ViewModel.ViewModel.ViewEditAddStateEnum)value;
                switch (ee)
                {
                    case MainVault.ViewModel.ViewModel.ViewEditAddStateEnum.View: return string.Empty;
                    case MainVault.ViewModel.ViewModel.ViewEditAddStateEnum.Edit: return "Edit Item";
                    case MainVault.ViewModel.ViewModel.ViewEditAddStateEnum.Add: return "Add New Item";
                }
            }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented");
        }
    }

    public class DateTimeSplitConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                var format = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat;
                var dt = (DateTime)value;
                return string.Format("{0}\n{1}", dt.ToString(format.ShortTimePattern), dt.ToString(format.ShortDatePattern));
            }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented");
        }
    }

}
