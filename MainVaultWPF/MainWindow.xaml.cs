﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MainVault.ViewModel;
using MainVault.Model;

namespace MainVault.View
{
    public partial class MainWindow : Window
    {
        MainVault.ViewModel.ViewModel _vm = null;
        public MainWindow()
        {
            InitializeComponent();
            _vm = new MainVault.ViewModel.ViewModel();
            this.DataContext = _vm;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_vm != null)
                _vm.Close();
        }

        private void KeyValueListView_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                KeyValueListView.Focus();
                Keyboard.Focus(KeyValueListView);
            }
        }
    }
}
