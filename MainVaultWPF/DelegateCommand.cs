﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace MainVault.ViewModel
{
    public class DelegateCommand<T>:ICommand
    {
        Action<T> _action=null;
        Func<T, bool> _canExec = null;
        public DelegateCommand(Action<T>action, Func<T,bool> canExec)
        {
            _action = action;
            _canExec = canExec;
        }

        public bool CanExecute(object parameter)
        {
            return _canExec((T)parameter);
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            _action((T)parameter);
        }
    }

    public class DelegateCommand : ICommand
    {
        Action _action = null;
        Func<bool> _canExec = null;
        public DelegateCommand(Action action, Func<bool> canExec)
        {
            _action = action;
            _canExec = canExec;
        }

        public bool CanExecute(object parameter=null)
        {
            return _canExec();
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter=null)
        {
            _action();
        }
    }

}
