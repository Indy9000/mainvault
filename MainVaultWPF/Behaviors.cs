﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Behaviors
{
    public class TextBoxBehavior
    {
        #region Attached Property EscapeClearsText

        public static readonly DependencyProperty EscapeClearsTextProperty
           = DependencyProperty.RegisterAttached("EscapeClearsText", typeof(bool), typeof(TextBoxBehavior),
                new FrameworkPropertyMetadata(false, new PropertyChangedCallback(OnEscapeClearsTextChanged)));

        private static void OnEscapeClearsTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                var textBox = d as TextBox;
                if (textBox != null)
                {
                    textBox.KeyUp -= TextBoxKeyUp;
                    textBox.KeyUp += TextBoxKeyUp;
                }
            }
        }

        private static void TextBoxKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                ((TextBox)sender).Text = string.Empty;
            }
        }

        public static void SetEscapeClearsText(DependencyObject dependencyObject, bool escapeClearsText)
        {
            if (!ReferenceEquals(null, dependencyObject))
                dependencyObject.SetValue(EscapeClearsTextProperty, escapeClearsText);
        }

        public static bool GetEscapeClearsText(DependencyObject dependencyObject)
        {
            if (!ReferenceEquals(null, dependencyObject))
                return (bool)dependencyObject.GetValue(EscapeClearsTextProperty);
            return false;
        }

        #endregion Attached Property EscapeClearsText

        #region Attached Property IsFocused
        public static readonly DependencyProperty IsFocusedProperty =
            DependencyProperty.RegisterAttached("IsFocused", typeof(bool), typeof(TextBoxBehavior),
             new UIPropertyMetadata(false, OnIsFocusedPropertyChanged));

        private static void OnIsFocusedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var uie = (UIElement)d;
            if ((bool)e.NewValue)
            {
                uie.Focus(); // Don't care about false values.
            }
        }

        public static bool GetIsFocused(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsFocusedProperty);
        }


        public static void SetIsFocused(DependencyObject obj, bool value)
        {
            obj.SetValue(IsFocusedProperty, value);
        }

        #endregion Attached Property IsFocused
    }

    public class AddToInputBinding
    {
        public static InputBinding GetBinding(DependencyObject obj)
        {
            return (InputBinding)obj.GetValue(BindingProperty);
        }

        public static void SetBinding(DependencyObject obj, InputBinding value)
        {
            obj.SetValue(BindingProperty, value);
        }
        public static readonly DependencyProperty BindingProperty
            = DependencyProperty.RegisterAttached("Binding", typeof(InputBinding), typeof(AddToInputBinding),
            new PropertyMetadata()
            {
                PropertyChangedCallback = (obj, e) =>
                {
                    ((UIElement)obj).InputBindings.Add((InputBinding)e.NewValue);
                }
            });
    }
}
