﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MainVault.Model;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;
using MainVaultWPF.Properties;

namespace MainVault.ViewModel
{
    /// <summary>
    /// ICONs are from : http://findicons.com/pack/2652/gentleface
    /// gentleface.com
    /// </summary>
    public class ViewModel : INotifyPropertyChanged
    {
        #region Implement INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property. 
        // The CallerMemberName attribute that is applied to the optional propertyName 
        // parameter causes the property name of the caller to be substituted as an argument. 
        private void Notify([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        public ViewModel()
        {
            LoadDatabase();
        }

        public enum ViewEnum
        {
            Home,Settings,About
        }

        ViewEnum _currentView = ViewEnum.Home;
        public ViewEnum CurrentView
        {
            get { return _currentView; }
            set { _currentView = value; Notify("CurrentView"); }
        }

        private void LoadDatabase()
        {
            try
            {
                if (System.IO.File.Exists(DatabaseFilePath))
                {
                    DataStore = JSONSerializer.Deserialize<DataStore>(DatabaseFilePath);
                    IsDatabaseDirty = false;
                }
            }
            catch (Exception eek)
            {
                Console.WriteLine("Error loading datastore {0}", eek.Message);
            }
        }

        bool _isDatabaseDirty = false;
        public bool IsDatabaseDirty
        {
            get { return _isDatabaseDirty; }
            set { _isDatabaseDirty = value; Notify("IsDatabaseDirty"); }
        }

        DataStore _store = new DataStore();
        public DataStore DataStore
        {
            get { return _store; }
            set
            {
                _store = value;
                if (_store == null)
                    CurrentView = ViewEnum.Settings;

                Notify("DataStore");
            }
        }

        DataNode _selectedDataNode = null;
        public DataNode SelectedDataNode
        {
            get { return _selectedDataNode; }
            set
            {
                _selectedDataNode = value;

                KeyValueState = (_selectedDataNode != null && _selectedDataNode.Data != null && _selectedDataNode.Data.Count == 0) ? ViewEditAddStateEnum.Add : ViewEditAddStateEnum.View;

                Notify("SelectedDataNode");
            }
        }

        bool _isNewKeyFocused = false;
        public bool IsNewKeyFocused
        {
            get { return _isNewKeyFocused; }
            set { _isNewKeyFocused = value; Notify("IsNewKeyFocused"); }
        }

        bool _isNewNodeFocused = false;
        public bool IsNewNodeFocused
        {
            get { return _isNewNodeFocused; }
            set { _isNewNodeFocused = value; Notify("IsNewNodeFocused"); }
        }

        public enum ViewEditAddStateEnum
        {
            View, Edit, Add
        }

        ViewEditAddStateEnum _nodeState = ViewEditAddStateEnum.View;
        public ViewEditAddStateEnum NodeState
        {
            get { return _nodeState; }
            set { _nodeState = value; Notify("NodeState"); }
        }

        ViewEditAddStateEnum _keyValueState = ViewEditAddStateEnum.View;
        public ViewEditAddStateEnum KeyValueState
        {
            get { return _keyValueState; }
            set { _keyValueState = value; Notify("KeyValueState"); }
        }

        KeyValueData _selectedKeyValueData;
        public KeyValueData SelectedKeyValueData
        {
            get { return _selectedKeyValueData; }
            set
            {
                if (_selectedKeyValueData != null)
                    _selectedKeyValueData.IsSelected = false;

                _selectedKeyValueData = value;

                if (_selectedKeyValueData != null)
                    _selectedKeyValueData.IsSelected = true;

                Notify("SelectedKeyValueData");
            }
        }

        private void SearchKey(string key)
        {
            if (key == null || key == string.Empty)
                return;

            Console.WriteLine("Search Key = {0}", key);
            //TODO: select this node and bring it to view
            var results = from n in DataStore.Nodes
                          where n.Name.ToUpper().Contains(key.ToUpper())
                          select n;
            if (results != null && results.Count() > 0)
            {
                SelectedDataNode = results.First();
                SearchTerm = string.Empty;
            }
            else
            {
                SelectedDataNode = null;
            }
        }

        private void IncrementalSearchKey(string key)
        {
            if (key == null || key == string.Empty)
                return;

            //TODO: select this node and bring it to view
            var results = from n in DataStore.Nodes
                          where n.Name.ToUpper().StartsWith(key.ToUpper())
                          select n;
            if (results != null && results.Count() > 0)
            {
                SelectedDataNode = results.First();
            }
            else
            {
                SelectedDataNode = null;
            }
        }

        string _searchTerm = string.Empty;
        public string SearchTerm
        {
            get { return _searchTerm; }
            set
            {
                _searchTerm = value;
                Notify("SearchTerm");

                IncrementalSearchKey(value);
            }
        }

        string _newNodeName = string.Empty;
        public string NewNodeName
        {
            get { return _newNodeName; }
            set { _newNodeName = value; Notify("NewNodeName"); }
        }

        string _newKey = string.Empty;
        public string NewKey
        {
            get { return _newKey; }
            set { _newKey = value; Notify("NewKey"); }
        }

        string _newValue = string.Empty;
        public string NewValue
        {
            get { return _newValue; }
            set { _newValue = value; Notify("NewValue"); }
        }

        string _databaseFilePath = Settings.Default.DatabaseFilePath;
        public string DatabaseFilePath
        {
            get { return _databaseFilePath; }
            set 
            {
                if (System.IO.File.Exists(value))
                {
                    Settings.Default.DatabaseFilePath = value;
                    Settings.Default.Save();
                }
                _databaseFilePath = value;
                Notify("DatabaseFilePath"); 
            }
        }

        //---------Key value data ----
        ICommand _addNewKeyValueDataCmd = null;
        public ICommand AddNewKeyValueDataCmd
        {
            get
            {
                return _addNewKeyValueDataCmd ?? (_addNewKeyValueDataCmd = new DelegateCommand(
                    () =>
                    {
                        KeyValueState = ViewEditAddStateEnum.Add;
                        NewKey = string.Empty;
                        NewValue = string.Empty;
                        IsNewKeyFocused = true;
                        IsDatabaseDirty = true;
                    },
                    () => true
                    ));
            }
        }

        ICommand _renameKeyValueDataCmd = null;
        public ICommand RenameKeyValueDataCmd
        {
            get
            {
                return _renameKeyValueDataCmd ?? (_renameKeyValueDataCmd = new DelegateCommand<KeyValueData>(
                    (kvp) =>
                    {
                        KeyValueState = ViewEditAddStateEnum.Edit;
                        NewKey = kvp.Key;
                        NewValue = kvp.TextValue;
                        IsNewKeyFocused = true;
                        IsDatabaseDirty = true;
                    },
                    (kvp) => kvp != null
                    ));
            }
        }

        ICommand _saveKeyValueDataCmd = null;
        public ICommand SaveKeyValueDataCmd
        {
            get
            {
                return _saveKeyValueDataCmd ?? (_saveKeyValueDataCmd = new DelegateCommand<string>(
                    (k) =>
                    {
                        if (KeyValueState == ViewEditAddStateEnum.Edit)
                        {
                            SelectedKeyValueData.Key = NewKey;
                            SelectedKeyValueData.TextValue = NewValue;
                        }
                        else
                        {
                            SelectedDataNode.Data.Add(new KeyValueData() { Key = NewKey, TextValue = NewValue });
                            NewKey = string.Empty;
                            NewValue = string.Empty;
                            IsNewKeyFocused = false;
                        }
                        KeyValueState = ViewEditAddStateEnum.View;
                        IsDatabaseDirty = true;
                    },
                    (k) => { return k != string.Empty; }
                    ));
            }
        }

        ICommand _cancelEditKeyValueDataCmd = null;
        public ICommand CancelEditKeyValueDataCmd
        {
            get
            {
                return _cancelEditKeyValueDataCmd ?? (_cancelEditKeyValueDataCmd = new DelegateCommand(
                    () =>
                    {
                        NewKey = string.Empty;
                        NewValue = string.Empty;
                        IsNewKeyFocused = false;
                        KeyValueState = ViewEditAddStateEnum.View;
                    },
                    () => true
                ));

            }
        }

        ICommand _deleteKeyValueDataCmd = null;
        public ICommand DeleteKeyValueDataCmd
        {
            get
            {
                return _deleteKeyValueDataCmd ?? (_deleteKeyValueDataCmd = new DelegateCommand<KeyValueData>(
                (kvd) =>
                {
                    SelectedDataNode.Data.Remove(kvd);
                    IsDatabaseDirty = true;
                },
                (kvd) =>
                {
                    return SelectedDataNode != null && SelectedDataNode.Data != null && kvd != null;
                }
                ));
            }
        }

        double _copyExpiryCounter = 0;
        public double CopyExpiryCounter
        {
            get { return _copyExpiryCounter; }
            set { _copyExpiryCounter = value; Notify("CopyExpiryCounter"); }
        }

        DispatcherTimer _clipboardClearTimer = null;

        ICommand _copyValueCmd = null;
        public ICommand CopyValueCmd
        {
            get
            {
                return _copyValueCmd ?? (_copyValueCmd = new DelegateCommand<KeyValueData>
                (
                (kvp) =>
                {
                    Debug.WriteLine(string.Format("Copied to Clipboard = {0}", kvp.TextValue));
                    Clipboard.SetText(kvp.TextValue);
                    CopyExpiryCounter = 100.0;
                    //Mouse.OverrideCursor = CreateCursor(50, 5, Brushes.Gold, null);
                    //the timer and animate the mouse cursor

                    //TODO: make the delay a configurable property
                    //Task.Delay(10000).ContinueWith(_ =>
                    //    {
                    //        Clipboard.SetText("");
                    //        Debug.WriteLine("Cleared Clipboard");
                    //    });

                    if (_clipboardClearTimer != null)
                        _clipboardClearTimer.Stop();
                    _clipboardClearTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
                    _clipboardClearTimer.Start();
                    _clipboardClearTimer.Tick += (sender, args) =>
                    {
                        CopyExpiryCounter -= 10.0;
                        if (CopyExpiryCounter <= 0.0)
                        {
                            _clipboardClearTimer.Stop();
                            Clipboard.SetText("");
                            Debug.WriteLine("Cleared Clipboard");
                        }
                    };
                },
                (kvd) => kvd.Key != null && kvd.TextValue != null
                ));
            }
        }

        //--------Node ops-------
        ICommand _renameNodeCmd = null;
        public ICommand RenameNodeCmd
        {
            get
            {
                return _renameNodeCmd ?? (_renameNodeCmd = new DelegateCommand<DataNode>(
                    (n) =>
                    {
                        NodeState = ViewEditAddStateEnum.Edit;
                        NewNodeName = n.Name;

                        IsNewNodeFocused = true;
                        IsDatabaseDirty = true;
                    },
                    (n) => n != null
                    ));
            }
        }

        ICommand _deleteNodeCmd = null;
        public ICommand DeleteNodeCmd
        {
            get
            {
                return _deleteNodeCmd ?? (_deleteNodeCmd = new DelegateCommand<DataNode>(
                (n) =>
                {
                    DataStore.Nodes.Remove(n);
                    IsDatabaseDirty = true;
                },
                (n) =>
                {
                    return DataStore != null && DataStore.Nodes != null && n != null;
                }
                ));
            }
        }

        ICommand _cancelEditNodeCmd = null;
        public ICommand CancelEditNodeCmd
        {
            get
            {
                return _cancelEditNodeCmd ?? (_cancelEditNodeCmd = new DelegateCommand(
                    () =>
                    {
                        NewNodeName = string.Empty;
                        IsNewNodeFocused = false;
                        NodeState = ViewEditAddStateEnum.View;
                    },
                    () => true
                ));

            }
        }

        ICommand _saveNodeCmd = null;
        public ICommand SaveNodeCmd
        {
            get
            {
                return _saveNodeCmd ?? (_saveNodeCmd = new DelegateCommand<string>(
                    (k) =>
                    {
                        if (NodeState == ViewEditAddStateEnum.Edit)
                        {
                            SelectedDataNode.Name = NewNodeName;
                            IsDatabaseDirty = true;
                        }
                        else
                        {
                            DataStore.Nodes.Add(new DataNode() { Name = NewNodeName });
                            NewNodeName = string.Empty;
                            IsNewNodeFocused = false;
                        }
                        NodeState = ViewEditAddStateEnum.View;
                    },
                    (k) => { return k != string.Empty; }
                    ));
            }
        }

        ICommand _addNewNodeCmd = null;
        public ICommand AddNewNodeCmd
        {
            get
            {
                return _addNewNodeCmd ?? (_addNewNodeCmd = new DelegateCommand(
                    () =>
                    {
                        NodeState = ViewEditAddStateEnum.Add;
                        NewNodeName = string.Empty;
                        IsNewNodeFocused = true;
                        IsDatabaseDirty = true;
                    },
                    () => true
                    ));
            }
        }

        ICommand _addNewNodeFromSearchBoxCmd = null;
        public ICommand AddNewNodeFromSearchBoxCmd
        {
            get
            {
                return _addNewNodeFromSearchBoxCmd ?? (_addNewNodeFromSearchBoxCmd = new DelegateCommand(
                    () =>
                    {
                        DataStore.Nodes.Add(new DataNode() { Name = SearchTerm });
                        SearchTerm = string.Empty;
                        IsNewNodeFocused = true;
                        IsDatabaseDirty = true;
                    },
                    () => true
                    ));
            }
        }

        //-------Search ops --- 
        ICommand _searchCmd = null;
        public ICommand SearchCmd
        {
            get
            {
                return _searchCmd ?? (_searchCmd = new DelegateCommand<string>(
                (searchTerm) =>
                {
                    SearchKey(searchTerm);
                },
                (searchTerm) => true//searchTerm != null && searchTerm!=string.Empty
                ));
            }
        }

        //--------Set Database--------------
        ICommand _setDatabaseCmd = null;
        public ICommand SetDatabaseCmd
        {
            get
            {
                return _setDatabaseCmd ?? (_setDatabaseCmd = new DelegateCommand(
                () =>
                {
                    //show file select
                    var dlg = new Microsoft.Win32.OpenFileDialog();
                    dlg.Multiselect = false;
                    dlg.DefaultExt = ".mvt";
                    dlg.Filter = "MainVault Database|*.mvt";
                    dlg.Title = "Select MainVault database to use";
                    Nullable<bool> result = dlg.ShowDialog();
                    if (result ?? false)
                    {
                        //Save current changes
                        if (IsDatabaseDirty)
                        {
                            JSONSerializer.Serialize<DataStore>(DatabaseFilePath, DataStore);
                            IsDatabaseDirty = false;
                        }

                        DatabaseFilePath = dlg.FileName;
                        //Load new
                        LoadDatabase();
                    }
                },
                () => true
                ));
            }
        }

        ICommand _setCurrentViewCmd = null;
        public ICommand SetCurrentViewCmd
        {
            get
            {
                return _setCurrentViewCmd ?? (_setCurrentViewCmd = new DelegateCommand<string>(
                    (p) =>
                    {
                        switch (p)
                        {
                            case "Home": CurrentView = ViewEnum.Home; break;
                            case "Settings": CurrentView = ViewEnum.Settings; break;
                        }
                    },
                    (p) => true
                    ));
            }
        }

        internal void Close()
        {
            JSONSerializer.Serialize<DataStore>(DatabaseFilePath, DataStore);
        }
    }
}
