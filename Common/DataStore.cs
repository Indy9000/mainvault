﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Security;
using System.Runtime.Serialization.Json;
using System.IO;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;

namespace MainVault.Model
{
    [DataContract]
    public class DataStore:INotifyPropertyChanged
    {
        #region Implement INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property. 
        // The CallerMemberName attribute that is applied to the optional propertyName 
        // parameter causes the property name of the caller to be substituted as an argument. 
        private void Notify([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        string _name = string.Empty;
        [DataMember]
        public string Name { get { return _name; } set { _name = value; Notify("Name"); } }

        ObservableCollection<DataNode> _nodes = new ObservableCollection<DataNode>();
        [DataMember]
        public ObservableCollection<DataNode> Nodes { get { return _nodes; } set { _nodes = value; Notify("Nodes"); } }
    }

    [DataContract]
    public class DataNode:INotifyPropertyChanged
    {
        #region Implement INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property. 
        // The CallerMemberName attribute that is applied to the optional propertyName 
        // parameter causes the property name of the caller to be substituted as an argument. 
        private void Notify([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        string _name = string.Empty;
        [DataMember]
        public string Name { get { return _name; } set { _name = value; Notify("Name"); } }

        DateTime _created = DateTime.Now;
        [DataMember]
        public DateTime Created { get { return _created; } set { _created = value; Notify("Created"); } }

        ObservableCollection<KeyValueData> _data = new ObservableCollection<KeyValueData>();
        [DataMember]
        public ObservableCollection<KeyValueData> Data { get { return _data; } set { _data = value; Notify("Data"); } }
    }

    [DataContract]
    public class KeyValueData : INotifyPropertyChanged
    {
        #region Implement INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property. 
        // The CallerMemberName attribute that is applied to the optional propertyName 
        // parameter causes the property name of the caller to be substituted as an argument. 
        private void Notify([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        string _key = string.Empty;
        [DataMember]
        public string Key { get { return _key; } set { _key = value; Notify("Key"); } }

        string _textValue = string.Empty;
        [DataMember]
        public string TextValue { get { return _textValue; } set { _textValue = value; Notify("TextValue"); } }

        bool _isSelected = false;
        [IgnoreDataMember]
        public bool IsSelected { get { return _isSelected; } set { _isSelected = value; Notify("IsSelected"); } }
    }

    public static class JSONSerializer
    {
        public static void Serialize<T>(string filename, T t)
        {
            using (var stream1 = new FileStream(filename,FileMode.Create,FileAccess.Write,FileShare.None))
            {
                var ser = new DataContractJsonSerializer(typeof(T));
                ser.WriteObject(stream1, t);
            }
        }

        public static T Deserialize<T>(string filename)
        {
            using(var stream = new FileStream(filename,FileMode.Open,FileAccess.Read,FileShare.Read))
            {
                stream.Seek(0, SeekOrigin.Begin);
                var ser = new DataContractJsonSerializer(typeof(T));
                var t = (T)ser.ReadObject(stream);
                return t;
            }
        }
    }
}


